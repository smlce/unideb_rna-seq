#!/bin/sh


## Master script for running RNA-seq analysis (trimming - alignment - counting - QC).
# Run it as:
# sh M_RNA-seq.sh <sample_name> <raw_file_R1> <raw_file_R2> <genome> <project>
# where
# <sample_name> is the unique name for your sample that you want to use throughout the analysis for various outputs;
# <raw_file_R1> is the name of the raw sequencing file you downloaded (read1 for PE sequencing or the only read file for SE sequencing);
# <raw_file_R2> is either 0 (in case of SE sequencing), or the name of the raw sequencing file you downloaded (read2 for PE sequencing); typing 0 here will trigger the SE pipeline;
# <genome> is the name and version of the organism and genome version of your sample, it is used for references for alignment, counting, annotation etc. (currently available: hg19);
# <project> is the full path of the project where the subfolders and output files will be created; the project folder should already exist and there should be a subfolder named "raw_data" that contains the gzipped raw fastq files.
# Example: sh M_RNA-seq.sh RNA_control_25 SRR5164887_1.fastq.gz SRR5164887_2.fastq.gz hg19 /data/workgroup/RNA-seq_project


## Set these variables manually:
# install_dir to the directory where this script (and any linked scripts) are located;
# index_dir to the directory where the indexes for alignment, counting etc. are located.

install_dir="/data10/working_groups/balint_group/m/programs/M_RNAseq"
index_dir="/data10/working_groups/balint_group/m/index"


## Start of the analysis.

date -R
echo "RNA-seq analysis script started."


## Reading command line arguments, setting up variables for the scripts.

sample_name=$1
raw_file_R1=$2
raw_file_R2=$3
genome=$4
project_dir=$5

if [ ${raw_file_R2} == 0 ]
then
echo "The data will be proceessed with the SE pipeline."
else
echo "The data will be proceessed with the PE pipeline."
fi


## Creating the folder structure.

cd ${project_dir}
mkdir ${project_dir}/qc
mkdir ${project_dir}/trimmed
mkdir ${project_dir}/aligned
mkdir ${project_dir}/counts


## Creating links for raw files (original files are left untouched).

cd ${project_dir}/raw_data
if [ ${raw_file_R2} == 0 ]
then
ln -s -f -T ${raw_file_R1} ${sample_name}.fq.gz
else
ln -s -f -T ${raw_file_R1} ${sample_name}_1.fq.gz
ln -s -f -T ${raw_file_R2} ${sample_name}_2.fq.gz
fi


## Preliminary QC.

cd ${project_dir}/qc
if [ ${raw_file_R2} == 0 ]
then
fastqc --noextract --nogroup -o ./ ${project_dir}/raw_data/${sample_name}.fq.gz
else
fastqc --noextract --nogroup -o ./ ${project_dir}/raw_data/${sample_name}_1.fq.gz
fastqc --noextract --nogroup -o ./ ${project_dir}/raw_data/${sample_name}_2.fq.gz
fi


## Trimming (standard Illumina adapters) and QC.

cd ${project_dir}/trimmed
if [ ${raw_file_R2} == 0 ]
then
trim_galore --fastqc --length 15 ${project_dir}/raw_data/${sample_name}.fq.gz
else
trim_galore --paired --fastqc --length 15 ${project_dir}/raw_data/${sample_name}_1.fq.gz ${project_dir}/raw_data/${sample_name}_2.fq.gz
fi


## Alignment (to genome, then transcriptome alignment is calculated).

cd ${project_dir}/aligned
if [ ${raw_file_R2} == 0 ]
then
STAR --readFilesCommand zcat --genomeDir ${index_dir}/star/genome/${genome} --sjdbGTFfile ${index_dir}/star/genome/${genome}/${genome}.gtf --sjdbOverhang 100 --readFilesIn ${project_dir}/trimmed/${sample_name}_trimmed.fq.gz --quantMode TranscriptomeSAM --quantTranscriptomeBAMcompression -1 --outSAMtype BAM SortedByCoordinate --outSAMunmapped Within --outFileNamePrefix ${sample_name}_
else
STAR --readFilesCommand zcat --genomeDir ${index_dir}/star/genome/${genome} --sjdbGTFfile ${index_dir}/star/genome/${genome}/${genome}.gtf --sjdbOverhang 100 --readFilesIn ${project_dir}/trimmed/${sample_name}_1_val_1.fq.gz ${project_dir}/trimmed/${sample_name}_2_val_2.fq.gz --quantMode TranscriptomeSAM --quantTranscriptomeBAMcompression -1 --outSAMtype BAM SortedByCoordinate --outSAMunmapped Within --outFileNamePrefix ${sample_name}_
fi

## RNA counting (transcription level quantification).

cd ${project_dir}/counts
if [ ${raw_file_R2} == 0 ]
then
rsem-calculate-expression --temporary-folder ./temp --strandedness reverse --seed-length 15 --no-bam-output --alignments ${project_dir}/aligned/${sample_name}_Aligned.toTranscriptome.out.bam ${index_dir}/rsem/transcriptome/${genome}/${genome} ${sample_name}
else
rsem-calculate-expression --temporary-folder ./temp --strandedness reverse --seed-length 15 --no-bam-output --paired-end --alignments ${project_dir}/aligned/${sample_name}_Aligned.toTranscriptome.out.bam ${index_dir}/rsem/transcriptome/${genome}/${genome} ${sample_name}
fi


## End of the analysis.

echo "RNA-seq analysis script finished."
date -R


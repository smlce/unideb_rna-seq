# Unideb_RNA-seq

RNA-seq pipeline for the BBL group at UD.

## Introduction

This is an RNA-seq pipeline which expects to start from Illumina sequenced raw fastq files, and it will perform the trimming, QC, alignment (to genome and transcriptome) and counting (by genes and transcripts).

For the usage of the comparative script, please see this section below: RNAseq_comparative

## Installation and setup

This a single shell script pipeline. Installation is as easy as cloning the the repository with git to your local computer. Check if you have privileges to write and to run the script. Open the script, and change the values of the "install_dir" and "index_dir" variables; these are the only two values you need to preset according to your local settings. "install_dir" is the directory where this script (and any linked scripts) are located; "index_dir" is the directory where the indexes for alignment, counting etc. are located. All the other values are set on the command line when you start the script.

However, the script has some prerequisites before it is ready to run, please check the next chapter.

### Prerequisites

To properly run the script, you need to have certain software tools and indexes ready on your computer. The software tools should be on your PATH so that the script can call them wherever hey are actually installed. The indexes are need to be in the "index_dir" directory.

### Software tools

The following tools are necessary to run the scipt (in brackets you can find the version this pipeline was tested with; newer versions should be backward compatible):

- FastQC (0.11.9)
- cutadapt (3.4)
- TrimGalore! (0.6.7)
- STAR (2.7.9a)
- RSEM (1.3.3)

Please check the project pages and the manuals of the respective software tools for installation and setup instructions. Be aware that I am not maintaining these software, for troubleshooting please contact their respective support contacts.

### Indexes

The script is currently recommended to process hg19 samples, but other organisms and versions can be used too. Acquire the appropriate files from a reliable database, e.g. Ensembl. The following files are needed:

- STAR: genome file in multifasta format, annotation file that contains all the genes and exons in gtf format
- RSEM: genome file in multifasta format, annotation file that contains all the genes and exons in gtf or gff3 format

Please follow the manuals of the respective software tools to prepare the genome and transcriptome indexes.

### Additional setup

Before you start the analysis, the project folder should already exist, and it should contain a directory named "raw_data". You should place your fastq files of the raw reads there. Everything else (including the rest of the folder structure) will be created automatically by the script.

## Running the script

Once everything is set up properly, you can run the script as any shell script (it is written in bash shell).

`sh M_RNA-seq.sh <sample_name> <raw_file_R1> <raw_file_R2> <genome> <project>`

The command line parameters of the script are the following:

**sample_name** is the unique name for your sample that you want to use throughout the analysis for various outputs

**raw_file_R1** is the name of the raw sequencing file you downloaded (read1 for PE sequencing or the only read file for SE sequencing); can be gzipped

**raw_file_R2** is either 0 (in case of SE sequencing), or the name of the (optionally gzipped) raw sequencing file you downloaded (read2 for PE sequencing); typing 0 here will trigger the SE pipeline

**genome** is the name and version of the organism and genome version of your sample, it is used for references for alignment, counting, annotation etc.

**project** is the name of the project, used for creating the project folder and generate the outputs in the subfolders of the project folder; in the project folder there should be a subfolder named "raw_data" that contains the gzipped raw fastq files

An example of running the script:

`sh M_RNA-seq.sh RNA_control_25 SRR5164887_1.fastq.gz SRR5164887_2.fastq.gz hg19 RNA-seq_project`

This would trigger the paired-end pipeline where "RNA_control_25" is the sample name, "SRR5164887_1.fastq.gz" and "SRR5164887_1.fastq.gz" are the raw read files in the "raw_data" subfolder, "hg19" is the genome/transcriptome/annotation version, and "RNA-seq_project" is the project name.

The script performs the following steps:

1. Creating the folder structure
2. Creating links to your raw read files with the appropriate sample names (the original files will be untouched)
3. Quality check with FastQC
4. Trimming the standard Illumina adapters with TrimGalore/cutadapt
5. Post-trimming QC with FastQC
6. Aligning the reads to the genome/transcriptome with STAR (providing both alignments)
7. Counting the reads with RSEM (providing both gene-level and isoform-level counts)

## Output and downstream analyses

The pipeline will create the following outputs from the aforementioned steps: trimmed read files, alignment files (to the genome and transcriptome), count files (per genes and transcripts), and QC reports.

The most relevant for the downstream analyses are the alignment files (in bam format) and the count files (in text format). With this information, knowing the gene expression levels, you are able to compare different sample groups, e.g. the RNA-seq profiles of healthy and diseased samples. These comparisons can range from visualizations, like hierarchical clustering and heatmaps, to determine if there is a consensus pattern of under-/overexpression, to searching for differentially expressed genes between sample groups with refined statistical tests. Several tools are available for these tasks, like various R packages (e.g. ggplot2) for visualization and tests, and EdgeR or DeSeq2 for identifying differentially enriched peaks.  
The genes of interest (e.g. the differentially expressed genes from the aforementioned comparison) can undergo a gene ontology/pathway analysis to identify the crucial signalling pathways, cell components, interactions etc. that are relevant for the research project, e.g. affected by a disease in a diseased-healthy expression profile comparison. There are offline and online tools for functional annotation and finding relevant pathways to your identified genes and proteins, e.g. Reactome, DAVID and STRING.

There are countless possibilities for downstream analyses, to get an overview of the full RNA-seq data analysis process and get inspired, please have a look at the SequencEnG website.

## Troubleshooting

For any bugs found feel free to create an issue in this project page.



# RNA-seq_comparative

## Introduction

This document describes the usage of the comparative script. It expects to start from RSEM gene level expression levels (can be produced by the main RNA-seq pipeline in this project), and if the proper setup is given (contrast of two sample groups), it will use EdgeR to normalize the samples, identify teh differentially expressed genes (DEGs), and produce 2 lists of DEGs annotated with Ensembl and Entrez IDs: one complete, unfiltered list and one list filtered by a q-value cutoff of 0.01 and Fold Change cutoff of 2.

## Installation and setup

The heart of the analysis is an R script, no need for installation, just copy it to your computer (you can clone the repository with git) and run the script. Check if you have privileges to write and to run.

However, the script has some prerequisites before it is ready to run, please check the next chapter.

### Prerequisites

To properly run the script, you need to have certain software and especially R packages installed on your computer.

### Software tools and packages

The following tools and packages are necessary to run the script (in brackets you can find the version this pipeline was tested with; newer versions should be backward compatible):

- R (4.1.1)
- edgeR (3.32.1)
- AnnotationDbi (1.52.0)
- org.Hs.eg.db (3.12.0)

Please check the project pages and the manuals of the respective software tools for installation and setup instructions. Be aware that I am not maintaining these software, for troubleshooting please contact their respective support contacts.

### Additional setup

Before you start the analysis, the count files should already exist; as it was mentioned before, the main pipeline can be used to generate that with RSEM from the raw fastq files. Note that the script will create the result files in the current folder, so it is wise to make a new folder inside the project folder, e.g. "comparison", and run the script from there.  
It is extremely important to set up the sample groups for the comparison properly. The script expects a single, headered csv file as its only argument, which should contain (in a comma separated format) the sample names, the ID of the sample group (0=control, 1=sample), and the path of the counts file corresponding to that sample. To ease the setup, the repository contains a template file with a 2 vs. 2 setup ("template.csv"). The header of the file will not be processed, it is only there as a guide for the user.

The contents of the template file, showing the comparison setup format:

`sample_name,control_sample,file_path control_rep1,0,/path/to/project/counts/control_rep1.genes.results control_rep2,0,/path/to/project/counts/control_rep2.genes.results sample_rep1,1,/path/to/project/counts/sample_rep1.genes.results sample_rep2,1,/path/to/project/counts/sample_rep2.genes.results`

## Running the script

Once everything is set up properly, you can run the script as any R script.

`Rscript M_RNA-seq_comparative.R <comparison_setup>`

The command line parameters of the script are the following:

**comparison_setup** is the name and full path of the csv file that contains the setup for the comparison: which samples belong to which group, and the path to their count files

An example of running the script:

`sh M_RNA-seq_comparative.R /RNA-seq_project/comparison/comparison_setup.csv`

This would compare the sample groups defined in the file "comparison_setup.csv", and produce the lists of DEGs in the current folder.

The script performs the following steps:

1. Filtering extreme values
2. Normalizing the counts
3. Testing for DEGs between the sample groups
4. Creating a complete DEG list and a DEG list filtered by q<0.01 and FC>2.
5. Annotating the genes with Ensembl and Entrez IDs
6. Generating the (tab separated) output files of both the filtered and unfiltered DEG lists

## Troubleshooting

For any bugs found feel free to create an issue in this project page.

